from django.db import models
from django.contrib.auth.models import User

class Estado(models.Model):
    nombre = models.CharField(max_length=45, null=False)

    def __str__(self):
        return self.nombre

    class Meta:
        app_label = 'app'

class Proyecto(models.Model):
    nombre = models.CharField(max_length=45, null=False)
    descripcion = models.TextField(null=True)
    productowner = models.ForeignKey(
        User,
        related_name='users_proyectos',
        null=False,
        on_delete=models.PROTECT)

    def __str__(self):
        return self.nombre

    class Meta:
        app_label = 'app'

class Tarea(models.Model):
    nombre = models.CharField(max_length=2000, null=False)
    descripcion = models.TextField(null=True)  # Field name made lowercase.
    proyecto = models.ForeignKey(
        Proyecto,
        related_name='proyectos_tareas',
        null=False,
        on_delete=models.PROTECT)
    developer = models.ForeignKey(
        User,
        related_name='users_tareas',
        null=False,
        on_delete=models.PROTECT)
    estado_actual = models.ForeignKey(
        Estado,
        related_name='estados_tareas',
        null=False,
        on_delete=models.PROTECT)
    tiempoestimado = models.IntegerField(null=False)  # Field name made lowercase.
    fechacreacion = models.DateTimeField(null=False)  # Field name made lowercase.

    def __str__(self):
        return self.nombre

    class Meta:
        app_label = 'app'

class Avance(models.Model):
    fecha = models.DateTimeField(null=False)
    descripcion = models.CharField(max_length=200, null=True)
    tiempotrabajado = models.IntegerField(null=False)  
    tiemporestante = models.IntegerField(null=False)
    tarea = models.ForeignKey(
        Tarea,
        related_name='tareas_avences',
        null=False,
        on_delete=models.PROTECT)
    usuario = models.ForeignKey(
        User,
        related_name='users_avences',
        null=False,
        on_delete=models.PROTECT)

    def __str__(self):
        return self.fecha

    class Meta:
        app_label = 'app'

class Developer(models.Model):
    usuario = models.ForeignKey(
        User,
        related_name='users_developer',
        null=False,
        on_delete=models.PROTECT)
    proyecto = models.ForeignKey(
        Proyecto,
        related_name='proyectos_developer',
        null=False,
        on_delete=models.PROTECT)
    activo = models.BooleanField(null=False)  # This field type is a guess.

    def __str__(self):
        return self.idusuario

    class Meta:
        app_label = 'app'