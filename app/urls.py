from django.urls import path 
from . import views 

app_name = 'app' 
urlpatterns = [
    path('', views.index, name='index'), 
    path('menu/', views.menu, name='menu'),
    path('charts/', views.charts, name='charts'),
    path('forgotpassword/', views.forgotpassword, name='forgotpassword'),
    path('register/', views.register, name='register'),
    path('registrar/', views.registrar, name='registrar'),
    path('autenticar/', views.autenticar, name='autenticar'),    
    path('logout/', views.view_logout, name='view_logout'),
    path('seeProyect/', views.seeProyect, name='seeProyect'),
    path('saveProyect/', views.saveProyect, name='saveProyect'),
    path('editProyect/', views.editProyect, name='editProyect'),
    path('EditEstado/', views.EditEstado, name='EditEstado'),
    path('EditTask/', views.EditTask, name='EditTask'),
    path('seeTask/', views.seeTask, name='seeTask'),
    path('addDeveloper/', views.addDeveloper, name='addDeveloper'),
    path('listdeveloper/', views.listdeveloper, name='listdeveloper'),
    path('addTarea/', views.addTarea, name='addTarea'),
    path('404/', views.notfound, name='404'),
    path('listdeveloper/eliminar/<int:id>/', views.view_eliminar_developer, name='view_eliminar_developer'),
    path('listtask/eliminar/<int:id>/', views.view_remove_developer, name='view_remove_developer'),
    path('listdeveloper/habilitar/<int:id>/', views.view_habilitar_developer, name='view_habilitar_developer'),
    path('listcoment/see/<int:id>/', views.view_see_developer, name='view_see_developer'),
    path('addComent/', views.addComent, name='addComent'),
]
